part of 'QrImport.dart';

class QRScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(150),
        child: Container(
          height: 100,
          //padding: const EdgeInsets.only(top: 30),
          alignment: Alignment.topCenter,
          decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
              image: AssetImage(Res.top),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            //margin: const EdgeInsets.only(bottom: 50),
            height: 300,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(Res.qr), fit: BoxFit.contain)),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: MyText(
              alien: TextAlign.center,
              size: 14,
              color: MyColors.primary,
              title:
                  'Please connect your device by scanning the QR code On The Device.',
            ),
          ),
          SizedBox(
            height: 40,
          ),
          MyGradientColor(title: 'Next', onTap: () {})
        ],
      ),
    );
  }
}
