import 'package:flutter/material.dart';

class BuildPic extends StatelessWidget {
  final String image;

  const BuildPic({required this.image});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 40),
      child: Image(
        height: 120,
        width: 140,
        image: AssetImage(image),
        fit: BoxFit.contain,
      ),
    );
  }
}
