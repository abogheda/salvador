import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/MyText.dart';
import 'package:flutter/material.dart';

class MyGradientColor extends StatelessWidget {
  final String title;
  final Function() onTap;
  final Color? textColor;
  final Color? color;
  final Color? borderColor;
  final BorderRadius? borderRadius;
  final EdgeInsets? margin;
  final double? width;

  MyGradientColor(
      {required this.title,
      required this.onTap,
      this.color,
      this.textColor,
      this.borderRadius,
      this.margin,
      this.borderColor,
      this.width});

  @override
  Widget build(BuildContext context) {
    Color border = color ?? MyColors.primary;
    return InkWell(
      onTap: onTap,
      child: Container(
          width: width ?? MediaQuery.of(context).size.width,
          height: 45,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                  colors: [MyColors.primary, MyColors.secondary])),
          margin: margin ?? EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Center(
            child: MyText(
              title: "$title",
              size: 11,
              color: textColor ?? MyColors.white,
            ),
          )),
    );
  }
}
