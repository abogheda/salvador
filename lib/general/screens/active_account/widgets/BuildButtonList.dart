part of 'ActiveAccountWidgetsImports.dart';

class BuildButtonList extends StatelessWidget {
  final ActiveAccountData activeAccountData;
  final String userId;

  const BuildButtonList(
      {required this.activeAccountData, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyGradientColor(
              width: 250,
              title: tr(context, "send"),
              onTap: () {
                // activeAccountData.onActiveAccount(context, userId),
                AutoRouter.of(context).push(ResetPasswordRoute(userId: ''));
              },
              // onTap: () => forgerPasswordData.onForgetPassword(context),
              textColor: MyColors.white,
              margin: const EdgeInsets.symmetric(vertical: 10),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              title: tr(context, "noReceiveCode"),
              size: 13,
              color: MyColors.grey,
            ),
            InkWell(
              onTap: () => activeAccountData.onResendCode(context, userId),
              child: MyText(
                  title: tr(context, "sendCode"),
                  size: 13,
                  color: MyColors.primary,
                  decoration: TextDecoration.underline),
            ),
          ],
        ),
      ],
    );
  }
}
