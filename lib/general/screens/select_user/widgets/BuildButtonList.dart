part of 'SelectUserWidgetsImports.dart';

class BuildButtonList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DefaultButton(
          title: tr(context, "patient"),
          onTap: () =>
              AutoRouter.of(context).push(RegisterRoute(isPatient: true)),
          margin: const EdgeInsets.symmetric(vertical: 15),
          color: MyColors.primary,
        ),
        DefaultButton(
          title: tr(context, "facilities"),
          onTap: () =>
              AutoRouter.of(context).push(RegisterRoute(isPatient: false)),
          margin: const EdgeInsets.symmetric(horizontal: 0),
          color: MyColors.secondary,
          borderColor: MyColors.primary,
          textColor: MyColors.white,
        ),
      ],
    );
  }
}
