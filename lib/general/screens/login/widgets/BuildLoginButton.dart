part of 'LoginWidgetsImports.dart';

class BuildLoginButton extends StatelessWidget {
  final LoginData loginData;

  const BuildLoginButton({required this.loginData});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        MyGradientColor(
          width: 120,
          title: tr(context, "login"),
          onTap: () => loginData.userLogin(context),
          textColor: MyColors.white,
          margin: const EdgeInsets.symmetric(vertical: 10),
        ),
      ],
    );
  }
}
