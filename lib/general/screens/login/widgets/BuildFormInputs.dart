part of 'LoginWidgetsImports.dart';

class BuildFormInputs extends StatelessWidget {
  final LoginData loginData;

  const BuildFormInputs({required this.loginData});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Form(
        key: loginData.formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconTextFiled(
              suffixIcon: Icon(
                Icons.phone,
                color: MyColors.primary,
              ),
              label: tr(context, "phone"),
              controller: loginData.phone,
              margin: const EdgeInsets.symmetric(vertical: 10),
              action: TextInputAction.next,
              type: TextInputType.number,
              validate: (value) => value!.validateEmpty(context),
            ),
            SizedBox(
              height: 20,
            ),
            IconTextFiled(
              suffixIcon: Icon(
                Icons.lock_outline,
                color: MyColors.primary,
              ),
              label: tr(context, "password"),
              controller: loginData.password,
              validate: (value) => value!.validateEmpty(context),
              isPassword: true,
              action: TextInputAction.done,
              submit: (value) => loginData.userLogin(context),
            ),
          ],
        ),
      ),
    );
  }
}
