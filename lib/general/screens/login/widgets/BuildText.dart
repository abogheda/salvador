part of 'LoginWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: Center(
            child: MyText(
              title: tr(context, "hello"),
              size: 16,
              color: MyColors.secondary,
            ),
          ),
        ),
        MyText(
          title: tr(context, "PleaseLoginToYourAccount"),
          size: 12,
          fontWeight: FontWeight.normal,
          alien: TextAlign.center,
          color: MyColors.secondary,
        ),
      ],
    );
  }
}
