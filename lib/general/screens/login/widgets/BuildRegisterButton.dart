part of 'LoginWidgetsImports.dart';

class BuildRegisterButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 70),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MyText(
            title: tr(context, "don'tHaveAccount"),
            size: 12,
            alien: TextAlign.center,
            color: MyColors.grey,
          ),
          InkWell(
            onTap: () {
              AutoRouter.of(context).push(SelectUserRoute());
            },
            child: MyText(
              title: tr(context, "register"),
              size: 12,
              alien: TextAlign.center,
              color: MyColors.secondary,
            ),
          ),
          // MyGradientColor(
          //   width: 100,
          //   title: tr(context, "register"),
          //   onTap: () {},
          //   color: MyColors.white,
          //   borderColor: MyColors.primary,
          //   textColor: MyColors.primary,
          //   margin: const EdgeInsets.symmetric(vertical: 10),
          // ),
        ],
      ),
    );
  }
}
