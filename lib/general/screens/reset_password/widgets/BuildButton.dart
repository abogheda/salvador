part of 'ResetPasswordWidgetsImports.dart';

class BuildButton extends StatelessWidget {
  final ResetPasswordData resetPasswordData;
  final String userId;

  const BuildButton({required this.resetPasswordData, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        MyGradientColor(
          width: 250,
          title: tr(context, "send"),
          onTap: () {
            //resetPasswordData.onResetPassword(context, userId),
            AutoRouter.of(context).push(LoginRoute());
          },
          // onTap: () => forgerPasswordData.onForgetPassword(context),
          textColor: MyColors.white,
          margin: const EdgeInsets.symmetric(vertical: 10),
        ),
      ],
    );
  }
}
