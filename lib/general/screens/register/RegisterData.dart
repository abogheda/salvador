part of 'RegisterImports.dart';

class RegisterData {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final GlobalKey<CustomButtonState> btnKey =
      new GlobalKey<CustomButtonState>();
  final TextEditingController name = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController password = new TextEditingController();
  final TextEditingController phone = new TextEditingController();
  final TextEditingController address = new TextEditingController();
  final TextEditingController confirmPassword = new TextEditingController();

  final GenericCubit<bool> registerCubit = new GenericCubit(false);
  final GenericCubit<bool> helmetCubit = new GenericCubit(false);

  void setUserRegister(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (formKey.currentState!.validate()) {
      // if (!registerCubit.state.data) {

      //   LoadingDialog.showSimpleToast(tr(context,
      //           "pleaseAccept") //"من فضلك قم بالموافقة علي الشروط والاحكام"
      //       );
      //   return;
      // }

      // btnKey.currentState.animateForward();
      // final String _token = await _firebaseMessaging.getToken();
      // SignUpModel model = new SignUpModel(
      //     name: name.text,
      //     phone: phone.text,
      //     email: email.text,
      //     password: password.text,
      //     deviceId: _token,
      //     deviceType: Platform.isIOS ? "ios" : "android");
      // await CustomerRepository(context: context).signUp(model);
      // btnKey.currentState.animateReverse();
    }
  }
}
