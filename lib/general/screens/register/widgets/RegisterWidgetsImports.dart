import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/general/blocks/generic_cubit/generic_cubit.dart';
import 'package:base_flutter/general/constants/Inputs/IconTextFiled.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/register/RegisterImports.dart';
import 'package:base_flutter/general/utilities/localization/LocalizationMethods.dart';
import 'package:base_flutter/general/utilities/routers/RouterImports.gr.dart';
import 'package:base_flutter/general/utilities/validator/Validator.dart';
import 'package:base_flutter/general/widgets/MyText.dart';
import 'package:base_flutter/general/widgets/myGradientColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'BuildCheckTerms.dart';
part 'BuildFormInputs.dart';
part 'BuildRegisterButton.dart';
part 'BuildText.dart';
part 'CheckHelmet.dart';
