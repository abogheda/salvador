part of 'RegisterWidgetsImports.dart';

class BuildRegisterButton extends StatelessWidget {
  final RegisterData registerData;

  const BuildRegisterButton({required this.registerData});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyGradientColor(
              width: 150,
              title: tr(context, "register"),
              onTap: () {
                // AutoRouter.of(context).push(DoneScreenRoute());
              },
              //registerData.setUserRegister(context),
              textColor: MyColors.white,
              margin: const EdgeInsets.symmetric(vertical: 10),
            ),
          ],
        ),
        // arguments: ActiveAccountArguments(userId: "b02276de-c148-4c6f-995a-965e5bf6cb3f")
        // ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              title: tr(context, 'haveAccount'),
              size: 11,
              color: MyColors.white,
            ),
            InkWell(
              onTap: () => AutoRouter.of(context).push(LoginRoute()),
              child: MyText(
                title: tr(context, 'login'),
                size: 11,
                color: MyColors.white,
              ),
            ),
          ],
        )
      ],
    );
  }
}
