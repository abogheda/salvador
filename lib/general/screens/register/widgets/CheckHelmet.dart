part of 'RegisterWidgetsImports.dart';

class BuildCheckHelmet extends StatelessWidget {
  final RegisterData registerData;

  const BuildCheckHelmet({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          margin:
              const EdgeInsets.only(left: 25, top: 10, right: 8, bottom: 10),
          width: Checkbox.width,
          height: Checkbox.width,
          child: BlocBuilder<GenericCubit<bool>, GenericState>(
            bloc: registerData.helmetCubit,
            builder: (context, state) {
              return Checkbox(
                materialTapTargetSize: MaterialTapTargetSize.padded,
                activeColor: MyColors.primary,
                checkColor: MyColors.white,
                value: state.data,
                onChanged: (val) =>
                    registerData.helmetCubit.onUpdateData(!state.data),
              );
            },
          ),
        ),
        InkWell(
          onTap: () {
            //ExtendedNavigator.of(context).push(Routes.terms);
          },
          child: MyText(
            title:
                tr(context, "haveHelmet"), //"قرأت و وافقت علي الشروط والاحكام",
            size: 10,
            color: MyColors.black,
          ),
        )
      ],
    );
  }
}
