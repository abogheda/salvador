part of 'RegisterWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0, left: 10, right: 10),
      child: MyText(
        title: tr(context, "register"),
        size: 25,
        fontWeight: FontWeight.bold,
        color: MyColors.secondary,
      ),
    );
  }
}
