part of 'RegisterWidgetsImports.dart';

class BuildFormInputs extends StatelessWidget {
  final RegisterData registerData;

  const BuildFormInputs({required this.registerData});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Form(
        key: registerData.formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconTextFiled(
              suffixIcon: Icon(
                Icons.person,
                color: MyColors.primary,
              ),
              label: tr(context, "name"),
              controller: registerData.name,
              margin: const EdgeInsets.symmetric(vertical: 7),
              action: TextInputAction.next,
              type: TextInputType.number,
              validate: (value) => value!.validateEmpty(context),
            ),
            IconTextFiled(
              suffixIcon: Icon(
                Icons.phone,
                color: MyColors.primary,
              ),
              label: tr(context, "phone"),
              controller: registerData.email,
              margin: const EdgeInsets.symmetric(vertical: 7),
              action: TextInputAction.next,
              type: TextInputType.number,
              validate: (value) => value!.validateEmpty(context),
            ),
            IconTextFiled(
              suffixIcon: Icon(
                Icons.email,
                color: MyColors.primary,
              ),
              label: tr(context, "mail"),
              controller: registerData.email,
              margin: const EdgeInsets.symmetric(vertical: 7),
              action: TextInputAction.next,
              type: TextInputType.number,
              validate: (value) => value!.validateEmpty(context),
            ),
            IconTextFiled(
              suffixIcon: Icon(
                Icons.lock_outline,
                color: MyColors.primary,
              ),
              margin: const EdgeInsets.symmetric(vertical: 7),
              label: tr(context, "password"),
              controller: registerData.password,
              validate: (value) => value!.validateEmpty(context),
              isPassword: true,
              action: TextInputAction.done,
              submit: (value) => registerData.setUserRegister(context),
            ),
            IconTextFiled(
              suffixIcon: Icon(
                Icons.lock_outline,
                color: MyColors.primary,
              ),
              margin: const EdgeInsets.symmetric(vertical: 7),
              label: tr(context, "confirmPassword"),
              controller: registerData.confirmPassword,
              validate: (value) => value!.validateEmpty(context),
              isPassword: true,
              action: TextInputAction.done,
              submit: (value) => registerData.setUserRegister(context),
            ),
          ],
        ),
      ),
    );
  }
}
