part of 'RegisterImports.dart';

class Register extends StatefulWidget {
  final bool isPatient;

  const Register({required this.isPatient});
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  RegisterData registerData = RegisterData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            physics: BouncingScrollPhysics(
              parent: AlwaysScrollableScrollPhysics(),
            ),
            children: [
              HeaderLogo(),
              BuildText(),
              BuildFormInputs(registerData: registerData),
              widget.isPatient
                  ? BuildCheckHelmet(
                      registerData: registerData,
                    )
                  : Container(
                      height: 1,
                    ),
              BuildCheckTerms(
                registerData: registerData,
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 100,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(Res.bottom), fit: BoxFit.cover)),
          child: BuildRegisterButton(
            registerData: registerData,
          ),
        ));
  }
}
