part of 'CheckPasswordWidgetsImports.dart';

class BuildText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, bottom: 35),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: MyText(
              title: tr(context, "codeHasBeenSent"),
              size: 18,
              color: MyColors.secondary,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Center(
            child: MyText(
              title: tr(context, "insertPhone"),
              size: 13,
              color: MyColors.primary,
            ),
          ),
        ],
      ),
    );
  }
}
