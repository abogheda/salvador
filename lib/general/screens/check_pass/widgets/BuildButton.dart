part of 'CheckPasswordWidgetsImports.dart';

class BuildButton extends StatelessWidget {
  final CheckPasswordData checkPasswordData;

  const BuildButton({required this.checkPasswordData});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        MyGradientColor(
          width: 250,
          title: tr(context, "login"),
          onTap: () {},
          //checkPasswordData.onForgetPassword(context),
          textColor: MyColors.white,
          margin: const EdgeInsets.symmetric(vertical: 10),
        ),
      ],
    );
  }
}
