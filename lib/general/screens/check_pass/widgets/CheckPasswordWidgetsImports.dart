import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/screens/check_pass/CheckPassImports.dart';
import 'package:base_flutter/general/utilities/localization/LocalizationMethods.dart';
import 'package:base_flutter/general/widgets/MyText.dart';
import 'package:base_flutter/general/widgets/myGradientColor.dart';
import 'package:flutter/material.dart';

part 'BuildButton.dart';
part 'BuildText.dart';
