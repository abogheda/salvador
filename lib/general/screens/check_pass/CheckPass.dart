part of 'CheckPassImports.dart';

class CheckPassword extends StatefulWidget {
  @override
  _CheckPasswordState createState() => _CheckPasswordState();
}

class _CheckPasswordState extends State<CheckPassword> {
  CheckPasswordData checkPasswordData = new CheckPasswordData();

  @override
  Widget build(BuildContext context) {
    return AuthScaffold(
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        physics: BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        children: [
          HeaderLogo(),
          BuildPic(image: Res.passed),
          BuildText(),
          // SizedBox(
          //   height: 15,
          // ),
          // BuildButton(forgerPasswordData: forgerPasswordData),
        ],
      ),
    );
  }
}
