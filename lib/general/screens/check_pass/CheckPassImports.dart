import 'package:base_flutter/general/screens/check_pass/widgets/CheckPasswordWidgetsImports.dart';
import 'package:base_flutter/general/widgets/AuthScaffold.dart';
import 'package:base_flutter/general/widgets/BuildPic.dart';
import 'package:base_flutter/general/widgets/HeaderLogo.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

part 'CheckPass.dart';
part 'CheckPassData.dart';
