part of 'ForgetPasswordWidgetsImports.dart';

class BuildButton extends StatelessWidget {
  final ForgerPasswordData forgerPasswordData;

  const BuildButton({required this.forgerPasswordData});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        MyGradientColor(
          width: 250,
          title: tr(context, "send"),
          onTap: () {
            AutoRouter.of(context)
                .push(ActiveAccountRoute(userId: '', isRegister: false));
          },
          // onTap: () => forgerPasswordData.onForgetPassword(context),
          textColor: MyColors.white,
          margin: const EdgeInsets.symmetric(vertical: 10),
        ),
      ],
    );
  }
}
