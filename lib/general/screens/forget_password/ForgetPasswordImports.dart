import 'package:base_flutter/general/constants/CustomButtonAnimation.dart';
import 'package:base_flutter/general/resources/GeneralRepository.dart';
import 'package:base_flutter/general/widgets/AuthScaffold.dart';
import 'package:base_flutter/general/widgets/BuildPic.dart';
import 'package:base_flutter/general/widgets/HeaderLogo.dart';
import 'package:base_flutter/res.dart';
import 'package:flutter/material.dart';

import 'widgets/ForgetPasswordWidgetsImports.dart';

part 'ForgetPassword.dart';
part 'ForgetPasswordData.dart';
